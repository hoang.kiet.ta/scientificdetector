from preprocessing.data_reader.DataReader import *
from preprocessing.video_extractor.VideoDownloader import *
from preprocessing import config


def main():
    data_name = config.config['data']
    data_path = select_data(data_name)
    video_data_path = data_path + '/video_data.csv'
    dir_path = os.path.dirname(os.path.realpath(__file__))
    path = dir_path + '/../res/videos'

    video_ids, labels = read_data(video_data_path)
    file_names = get_file_names(path)

    if config.config['video_extractor']['download_videos']:
        for video_id in video_ids:
            download(video_id, path, file_names)
    if config.config['video_extractor']['create_video_list']:
        create_video_list(path, video_ids, labels, file_names)


if __name__ == '__main__':
    main()
