config = {
    "data": 'Serrano',
    "video_extractor": {
        'create_video_list': False,
        'download_videos': True,
    },
    "audio_extractor": {
        'extract_audio': True,
        'split': False,
    },
    "transcriber": {
        'transcribe': True,
        'recombine': False,
        'json': False,
        'punctuate': True,
    },
    "youtube_api_key": '<Insert YouTube API Key>',
    "credential_file": '<Insert the credential file>',
}
