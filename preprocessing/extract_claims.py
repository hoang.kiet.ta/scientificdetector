import os
from model.claim_extractor.scripts.predict import predict
from model.claim_extractor.scripts.eval import evaluate



def main():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    transcription_path = os.path.join(dir_path, '../res/claims/transcripts')
    output_path = os.path.join(dir_path, '../res/claims/potential_claims/claim_extractor/claims.json')
    test_path = os.path.join(dir_path, '../res/claims/potential_claims/cleaned_claims.json')

    predict(transcription_path, output_path)
    #evaluate(test_path)


if __name__ == '__main__':
    main()
