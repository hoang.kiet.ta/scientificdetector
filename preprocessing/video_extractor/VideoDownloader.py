import pytube
import os
import io
import pandas as pd
import re
import time
import google_auth_oauthlib.flow

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaIoBaseDownload
from preprocessing import config
from pytube.cli import on_progress
from pytube.exceptions import RegexMatchError


def get_file_names(path: str) -> [str]:
    file_names = list()

    for file in os.listdir(path):
        if file.endswith('.mp4'):
            file_names.append(file)

    return file_names


def download(video_id: str, video_path: str, file_names: [str]):
    video_url = 'http://www.youtube.com/watch?v=' + video_id
    try:
        yt = pytube.YouTube(video_url, on_progress_callback=on_progress)
        video = yt.streams.first()
        file_name = yt.title + '.mp4'
        if file_name not in file_names:
            video.download(output_path=video_path, filename=yt.title)
            print(yt.title)
        else:
            print(yt.title, 'skipped!')
        time.sleep(5)
        return True
    except RegexMatchError:
        print(video_id, 'video not found!')
        time.sleep(5)
        return False


def extract_captions(text_path, video_ids, language='en'):
    for video_id in video_ids:
        video_url = 'http://www.youtube.com/watch?v=' + video_id
        try:
            yt = pytube.YouTube(video_url)
            filename = yt.title + '-caption'
            caption = yt.captions.get_by_language_code(language)
            if caption:
                with open(text_path + '/' + filename + '-caption.txt', mode='w+') as file:
                    file.write(caption.generate_srt_captions())
                    file.write('\n')
                    file.close()
        except RegexMatchError:
            print('No video match video id')


def create_video_list(path, video_ids, labels, file_names):

    next_titles = list()
    next_video_ids = list()
    next_labels = list()

    skipped_titles = list()
    skipped_ids = list()
    skipped_labels = list()

    no_match_ids = list()
    no_match_labels = list()

    index = 0
    list_count = 0

    columns = ['video_id', 'title', 'misinformation']

    for video_id in video_ids:
        try:
            video_url = 'http://www.youtube.com/watch?v=' + video_id
            yt = pytube.YouTube(video_url)
            title = yt.title + '.mp4'
            title = re.sub(r'[\\/*?:"<>|;,\'#~´$]', "", title)
            time.sleep(5)
            if title in file_names:
                next_titles.append(yt.title)
                next_video_ids.append(video_id)
                next_labels.append(labels.iat[index])
                list_count += 1
                print(title, list_count)
                time.sleep(10)
            else:
                skipped_titles.append(title)
                skipped_ids.append(video_id)
                skipped_labels.append(labels.iat[index])
                print(title, 'skipped')
        except RegexMatchError:
            no_match_ids.append(video_id)
            no_match_labels.append(labels.iat[index])
            print('No Match', video_id)
            time.sleep(10)
        print(index)
        index += 1
    df = pd.DataFrame(
        zip(next_video_ids, next_titles, next_labels),
        columns= columns
    )
    df_skipped = pd.DataFrame(
        zip(skipped_ids, skipped_titles, skipped_labels),
        columns=columns
    )
    df_missed = pd.DataFrame(
        zip(no_match_ids, no_match_labels),
        columns=columns
    )
    df.to_csv(path + '/video_list.csv')
    df_skipped.to_csv(path + '/skipped.csv')
    df_missed.to_csv(path + '/missed.csv')


def extract_captions_ids(video_ids, path, start=0, count=50):
    youtube = build('youtube', 'v3', developerKey=config.config['youtube_api_key'])

    captions = list()
    ids = list()
    index = start
    for video_id in video_ids:
        if index > start + count:
            break
        try:
            req = youtube.captions().list(
                part='snippet',
                videoId=video_id
            )
            res = req.execute()
            if len(res['items']) > 0:
                caption = res['items'][0]['id']
                captions.append(caption)
                ids.append(video_id)
                index += 1
        except HttpError:
            print('HttpError')

    df = pd.DataFrame(zip(ids, captions), columns=['video_id', 'caption_id'])
    df.to_csv(path + '/caption_list.csv')


def download_captions(path, start=0, count=1):

    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
    scopes = ["https://www.googleapis.com/auth/youtube.force-ssl"]

    api_service_name = "youtube"
    api_version = "v3"
    client_secrets_file = config.config['credential_file']
    # Get credentials and create an API client
    flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file(
        client_secrets_file, scopes)
    credentials = flow.run_console()
    youtube = build(
        api_service_name, api_version, credentials=credentials)

    caption_list = pd.read_csv(path + '/caption_list.csv')
    captions = caption_list['caption_id']
    index = start

    #for caption in captions:
    #    if index > start + count:
    #        break
    #    print(caption)
    #    request = youtube.captions().download(id=caption)

    request = youtube.captions().download(id='akl6s-2PBdV5bGTufoZ-61_od6FMGaHRI1j6g9RB7hs=')
    fh = io.FileIO(path + '/' + str(index) + '.txt', 'wb')

    index += 1
    download_file = MediaIoBaseDownload(fh, request)
    complete = False
    while not complete:
        #try:
        status, complete = download_file.next_chunk()
        #except HttpError:
        #    print('HTTP Error')
        #    break
