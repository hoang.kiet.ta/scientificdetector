from googleapiclient.discovery import build


class VideoFinder:

    def __init__(self):
        api_key = 'AIzaSyCINZ9JTzkAfanUJeIzqtwWHDzaDeE9MEc'
        self.youtube = build('youtube', 'v3', developerKey=api_key)

        self.videos = list()
        self.captions = list()

    def request(self, keyword):
        req = self.youtube.search().list(
            part='snippet',
            q=keyword,
            type='video',
            maxResults=50
        )

        res = req.execute()

        return res

    def extract_videos(self, res):
        for item in res['items']:
            video = {
                'title': item['snippet']['title'],
                'id': item['id']['videoId']
            }
            print(video)
            self.videos.append(video)
