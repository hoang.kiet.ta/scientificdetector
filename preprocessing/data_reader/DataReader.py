import pandas as pd
import os


def select_data(data_name: str) -> str:

    dir_path = os.path.dirname(os.path.realpath(__file__))
    data_path = dir_path + '/../../data/' + data_name

    return data_path


def read_data(path: str) -> ([str], [str]):
    data = pd.read_csv(path)

    return data['video_id'], data['misinformation']
