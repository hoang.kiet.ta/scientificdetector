import json
import os


def convert_text_to_json(path):
    dir_path = os.path.dirname(os.path.realpath(__file__))

    file_names = list()
    for file in os.listdir(path):
        if file.endswith('.txt'):
            file_names.append(file)
    transcripts = []
    for file_name in file_names:
        f = open(path + '/' + file_name, encoding='utf-8')
        text = f.read()
        title = file_name.split('-transcribed.')[0]
        json_data = {
            "title": title,
            "transcripts": text
        }
        transcripts.append(json_data)
    json_out = {
        'data': transcripts
    }
    with open(f'{dir_path}/../../res/claims/transcripts/transcripts.json', 'w') as f:
        json.dump(json_out, f)


if __name__ == '__main__':
    dir_path = os.path.dirname(os.path.realpath(__file__))
    convert_text_to_json(f'{dir_path}/../../res/claims/transcripts')
