import json
import os
import random


def main():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    input_path = f'{dir_path}/../../res/claims/cleaned_claims'
    output_path = f'{dir_path}/../../res/claims/potential_claims/cleaned_claims.json'

    data = open(input_path, 'r')
    lines = data.read().split('\n')

    count = 0
    sentences = []
    for line in lines:
        worthy = 1
        if count > 100:
            worthy = 0
        sentence = {
            'claim': line,
            'checkworthy': worthy
        }
        sentences.append(sentence)
        count += 1
    random.shuffle(sentences)
    with open(output_path, 'w') as fw:
        json.dump(sentences, fw)
        fw.close()


if __name__ == '__main__':
    main()
