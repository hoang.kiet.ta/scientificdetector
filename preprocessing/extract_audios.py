from preprocessing.audio_extractor.AudioExtractor import *
from preprocessing import config


def main():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    video_path = dir_path + '/../res/videos'
    audio_path = dir_path + '/../res/audios'
    split_audio_path = dir_path + '/../res/error/too_big'

    audio_config = config.config['audio_extractor']

    if audio_config['extract_audio']:
        extract_audio(video_path, audio_path)
    if audio_config['split']:
        split_audio(split_audio_path)


if __name__ == '__main__':
    main()
