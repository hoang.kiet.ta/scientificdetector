import os
import moviepy.editor as me
from pydub import AudioSegment
from pydub.silence import split_on_silence

"""

extract audio from video resources and save it as a wav-file

@:param path - video source path
@:param audio_path - target audio path
"""


def extract_audio(path: str, audio_path: str):
    print('Extract audio...')
    file_names = list()

    for file in os.listdir(path):
        if file.endswith('.mp4'):
            file_names.append('/' + file)

    for file_name in file_names:
        video = me.VideoFileClip(path + file_name)
        audio = video.audio
        print(audio_path + file_name.split('.')[0] + '.wav')
        audio.write_audiofile(audio_path + file_name.split('.')[0] + '.wav')


def split_audio(path: str):
    print('Split audio files...')
    file_names = list()

    for file in os.listdir(path):
        if file.endswith('.wav'):
            file_names.append('/' + file)
    for file_name in file_names:
        print(path + file_name)
        sound = AudioSegment.from_file(path + file_name, format='wav')
        print('Building chunks...')
        chunks = split_on_silence(
            sound,

            # split on silences longer than 1000ms (1 sec)
            min_silence_len=200,

            # anything under -16 dBFS is considered silence
            silence_thresh=-16,

            # keep 200 ms of leading/trailing silence
            keep_silence=2000
        )

        # now recombine the chunks so that the parts are at least 90 sec long
        target_length = 20 * 60 * 1000
        try:
            output_chunks = [chunks[0]]
            print('Building output audio...')
            for chunk in chunks[1:]:
                print('(' + str(len(output_chunks[-1])) + '/' + str(target_length) + ')')
                if len(output_chunks[-1]) < target_length:
                    output_chunks[-1] += chunk
                else:
                    # if the last output chunk is longer than the target length,
                    # we can start a new one
                    output_chunks.append(chunk)

            for i, out_chunk in enumerate(output_chunks):
                chunk_name = path + '/splitted' + file_name.split('.wav')[0] + "-{0}.wav".format(i)
                print("exporting", chunk_name)
                out_chunk.export(chunk_name, format="wav")
        except IndexError:
            print('Nothing to split!')
