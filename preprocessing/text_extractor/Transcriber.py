import os
import time
import speech_recognition as sr
import json

from ibm_watson import SpeechToTextV1
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator
from http.client import BadStatusLine

'''

transcribe audio file into readable text and save it as txt-file

@:param path - audio source path
@:param text_path - target transcribed text path
'''


def transcribe(path):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    cred_path = os.path.join(dir_path, '..', 'cred', 'ibm_cred.json')
    with open(cred_path) as json_file:
        cred = json.load(json_file)
    authenticator = IAMAuthenticator(cred['apikey'])
    service = SpeechToTextV1(authenticator=authenticator)
    service.set_service_url(cred['url'])

    file_names = list()

    for file in os.listdir(path):
        if file.endswith('.wav'):
            file_names.append(file)

    for file_name in file_names:
        # creating audio object
        audio_file_name = path + '/' + file_name
        print(file_name)
        with open(audio_file_name, 'rb') as audio_file:
            speech_recognition_results = service.recognize(
                audio=audio_file,
                content_type='audio/wav'
            ).get_result()
        # transcribe the audio file
        with open(dir_path + '/../../res/json/ibm/finished/' + file_name.split('.')[0] + '.json', 'w') as fp:
            fp.write(json.dumps(speech_recognition_results, indent=2))


def transcribe_sphinx(path, text_path):

    recognizer = sr.Recognizer()

    file_names = list()

    for file in os.listdir(path):
        if file.endswith('.wav'):
            file_names.append(file)

    for file_name in file_names:
        # creating audio object
        audio = sr.AudioFile(path + '/' + file_name)
        print(file_name)
        with audio as source:
            audio_file = recognizer.record(source)
        # transcribe the audio file
        result = ""
        try:
            result = recognizer.recognize_sphinx(audio_file)
            print(result)
            time.sleep(10)
        except sr.UnknownValueError:
            print("Google Speech Recognition could not understand audio")
        except sr.RequestError as e:
            print("Could not request results from Google Speech Recognition service; {0}".format(e))
        except BadStatusLine:
            print("could not fetch Google URL")
        if len(result) > 0:
            with open(text_path + '/' + file_name.split('.')[0] + '-transcribed.txt', mode='w+') as file:
                file.write(result)
                file.write('\n')
                file.close()


def recombine_splitted(path):
    file_names = list()
    files = list()

    for file in os.listdir(path):
        if file.endswith('.txt'):
            files.append(file)
        if file.endswith('-0-transcribed.txt'):
            filename = file.split('-0-transcribed.txt')[0]
            file_names.append(filename)

    for filename in file_names:
        result_text = ''
        for file in files:
            if file.startswith(filename):
                f = open(path + '/' + file, mode='r+')
                text = f.read()
                result_text += text
        with open(path + '/../need_to_processed/' + filename + '-transcribed.txt', mode='w+') as w:
            w.write(result_text)
