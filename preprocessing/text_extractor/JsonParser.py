import json
import os


def extract_subtitles(path, text_path):
    file_names = list()

    for file in os.listdir(path):
        if file.endswith('.json'):
            file_names.append(file)

    for file_name in file_names:
        f = open(path + '/' + file_name, encoding='utf-8')
        data = json.load(f)
        result = ''
        for event in data['events']:
            if 'segs' in event:
                for seg in event['segs']:
                    result += seg['utf8']
        with open(text_path + '/' + file_name.split('.')[0] + '-transcribed.txt', mode='w+', encoding='utf-8') as file:
            file.write(result)
            file.close()
        f.close()


def extract_subtitles_ibm(path):
    file_names = list()
    dir_path = os.path.dirname(os.path.realpath(__file__))
    processed_path = os.path.join(dir_path, '..', '..', 'res', 'json', 'ibm', 'processed')

    for file in os.listdir(path):
        if file.endswith('.json'):
            file_names.append(file)
    print(file_names)
    for file_name in file_names:
        f = open(path + '/' + file_name, encoding='utf-8')
        data = json.load(f)
        result = ''
        for text in data['results']:
            result += text['alternatives'][0]['transcript']
        print(result)
        with open(processed_path + '/' + file_name.split('.')[0] + '.txt', mode='w+', encoding='utf-8') as file:
            file.write(result)
            file.close()
        f.close()


def convert_to_json(path):
    dir_path = os.path.dirname(os.path.realpath(__file__))

    file_names = list()
    for file in os.listdir(path):
        if file.endswith('.txt'):
            file_names.append(file)
    transcripts = []
    for file_name in file_names:
        f = open(path + '/' + file_name, encoding='utf-8')
        text = f.read()
        title = file_name.split('.')[0]
        json_data = {
            "title": title,
            "transcripts": text
        }
        transcripts.append(json_data)
    json_out = {
        'data': transcripts
    }
    with open(f'{dir_path}/../../res/transcripts/json/transcripts.json', 'w') as f:
        json.dump(json_out, f)
