from preprocessing.text_extractor.Transcriber import *
from preprocessing.text_extractor.JsonParser import *
from preprocessing import config

from model.punctuator.punctuator import punctuate
from model.punctuator.convert_to_readable import convert_to_readable


def main():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    audio_path = dir_path + '/../res/audios'
    text_path = dir_path + '/../res/transcripts'
    json_path_yt = dir_path + '/../res/json/youtube'
    json_path_ibm = dir_path + '/../res/json/ibm/finished'

    transcript_path = dir_path + '/../res/json/ibm/processed'
    recombine_path = text_path + '/splitted'
    punctuation_path = dir_path + '/../res/json/ibm/punctuated'
    readable_path = dir_path + '/../res/json/ibm/readable'

    transcriber_config = config.config['transcriber']

    if transcriber_config['transcribe']:
        transcribe(audio_path)
    if transcriber_config['recombine']:
        recombine_splitted(recombine_path)
    if transcriber_config['json']:
        extract_subtitles_ibm(json_path_ibm)
    if transcriber_config['punctuate']:
        model = '../model/punctuator/pretrained_models/Demo-Europarl-EN.pcl'
        punctuate(model, transcript_path, punctuation_path)
        convert_to_readable(processed_path=punctuation_path, readable_path=readable_path)
        convert_to_json(readable_path)


if __name__ == '__main__':
    main()
