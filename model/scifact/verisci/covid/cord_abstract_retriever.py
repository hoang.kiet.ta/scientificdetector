import json
import spacy
import os


class CordAbstractRetriever:

    def __init__(self):
        self.nlp = spacy.load("en_core_sci_sm")

    def __call__(self, claim: str, k=20):
        print("Retrieving abstracts.")
        dir_path = os.path.dirname(os.path.realpath(__file__))
        data_path = dir_path + '/../../data/medrxiv_biorxiv/corpus.json'

        with open(data_path, encoding='utf-8') as f:
            dataset = json.load(f)

        out = []
        for data in dataset['rels']:
            blob = {
                'id': data['rel_doi'].split('.')[-1],  # str
                'title': data['rel_title'],  # str
                'abstract': self._sentencize(data['rel_abs']),  # List[str]
                'journal': data['rel_site'],  # str
                'url': 'https://api.semanticscholar.org/' + data['rel_doi'],
                'authors': data['rel_authors'],  # List[str]
            }
            out.append(blob)
        return out[:k]

    def _sentencize(self, text):
        doc = self.nlp(text)
        return [sent.text for sent in doc.sents]
