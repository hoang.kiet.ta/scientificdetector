import json
import os
import argparse

from tqdm import tqdm
from model.scifact.verisci.covid.MedrxivRetriever import MedrxivRetriever
from urllib.error import URLError, HTTPError
from urllib.parse import quote


def get_args():
    parser = argparse.ArgumentParser(
        description="Verify a claim against the CORD-19 corpus.")
    parser.add_argument("--coref", action="store_true", help="retrieve evidences from coref claims")

    return parser.parse_args()


def retrieve_data(n: int, claim: str, negated: bool):
    br = MedrxivRetriever()
    print(claim)
    try:
        matched_papers = br.query(
            query=quote(claim),
            n=n,
            metadata=True,
            full_text=False,
        )
    except HTTPError:
        return {"claim": claim, "sources": []}
    cleaned_papers = clean_data(matched_papers)

    dir_path = os.path.dirname(os.path.realpath(__file__))
    json_data = {
        "claim": claim,
        "sources": cleaned_papers,
        "negated": negated
    }
    return json_data


def clean_data(papers):
    out = []

    for paper in papers:
        url = paper['medrxiv_url']
        url_elems = url.split('/')
        doi_elem = url_elems[-1]
        doi = f'{url_elems[-2]}/{doi_elem[:-2].split(".")[-1]}'
        try:
            paper_data = {
                "title": paper['title'],
                "abstract": paper['abstract'],
                "url": url,
                "doi": doi
            }
        except KeyError:
            paper_data = {
                "title": paper['title'],
                "abstract": '',
                "url": url,
                "doi": doi
            }
        out.append(paper_data)

    return out


def retrieve(args):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    data_out = []

    if args.coref:
        with open(f'{dir_path}/../../../res/claims/potential_claims/negated_claims.json', encoding='utf-8') as f:
            data_input = json.load(f)
            f.close()
        for data in tqdm(data_input):
            evidences = []
            negated = False
            if data['changed']:
                claim = data['claim']
                try:
                    result = retrieve_data(10, claim, data['negated_claim'])
                    evidences.append(result)
                except HTTPError:
                    continue
                except URLError:
                    continue
                if data['negated_claim']:
                    negated = True
                out_data = {
                    "evidences": evidences,
                    "negated": negated
                }
                data_out.append(out_data)
    else:
        backup_path = f'{dir_path}/../data/medrxiv/backup.json'
        if os.path.exists(backup_path):
            with open(backup_path, encoding='utf-8') as backup:
                data_out = json.load(backup)
                backup.close()
        with open(f'{dir_path}/../../../res/claims/potential_claims/cleaned_claims.json', encoding='utf-8') as f:
            json_output = json.load(f)
            data_input = json_output['data']
            f.close()
        count = 1
        cursor = len(data_out)
        for data in tqdm(data_input):
            if count < cursor:
                continue
            evidences = []
            claim_datas = data['claims']
            for claim_data in tqdm(claim_datas):
                if claim_data['checkworthy'] == 1:
                    try:
                        result = retrieve_data(10, claim_data['claim'])
                        evidences.append(result)
                    except HTTPError:
                        continue
                    except URLError:
                        continue
            out_data = {
                "video_title": data['title'],
                "evidences": evidences,
            }
            data_out.append(out_data)
            print(count)
            with open(f'{dir_path}/../data/medrxiv/backup.json', 'w') as fw:
                json.dump(data_out, fw)
                fw.close()
            count += 1

    if args.coref:
        output_path = f'{dir_path}/../data/medrxiv/evidences_coref.json'
    else:
        output_path = f'{dir_path}/../data/medrxiv/evidences.json'
    with open(output_path, 'w') as fw:
        json.dump(data_out, fw)
        fw.close()


if __name__ == "__main__":
    args = get_args()
    retrieve(args)
