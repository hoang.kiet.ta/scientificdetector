import json
import jsonlines
import os
import argparse

from statistics import mean, stdev, median
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score
from tqdm import tqdm

labels = {
    'REFUTE': -1,
    'NOT_ENOUGH_INFO': 0,
    'SUPPORT': 1
}


def get_args():
    parser = argparse.ArgumentParser(
        description="Evaluate results")
    parser.add_argument("--evidences", type=str, choices=["arxiv", "biorxiv", "medrxiv"], default="none")

    return parser.parse_args()


def get_file_names(path: str) -> [str]:
    file_names = list()

    for file in os.listdir(path):
        if file.endswith('.json'):
            file_names.append(file)

    return file_names


def get_split(indices, results):
    test_set = []
    for index in indices:
        test_set.append(results[index])
    return test_set


def load_annotation(args, annotation_path):
    with open(annotation_path, encoding='utf-8') as fc:
        annotations = list()
        annotation_ids = list()
        for annotation in jsonlines.Reader(fc):
            annotation_dict = dict([
                (x, y)
                for x, y in zip(annotation['anno'].keys(), annotation['anno'].values()) if args.evidences in x
            ])
            annotations.append({
                'claim_id': annotation['claim_id'],
                'anno': annotation_dict
            })
            annotation_ids.append(annotation['claim_id'])
        fc.close()
    return annotations, annotation_ids


def load_data(result_path, annotation_ids):
    with open(result_path, encoding='utf-8') as f:
        results = json.load(f)['results'][0]['evidences']
        results = [
            {'id': i, 'claim': x['claim'], 'evidences': x['evidences']}
            for x, i in zip(results, range(len(results)))
        ]
        f.close()

    results = [x for x in results if x['id'] in annotation_ids]
    return results


def filter_evidences(result, annotations_keys):
    if len(result['evidences']) > 0:
        filtered_evidences = [x for x in result['evidences'] if x['url'] in annotations_keys]
        difference = len(annotations_keys) - len(filtered_evidences)
        if difference > 0:
            filtered_evidences += [{'label': 'NOT_ENOUGH_INFO'}] * difference
    else:
        filtered_evidences = [{'label': 'NOT_ENOUGH_INFO'}] * len(annotations_keys)
    return filtered_evidences


def main():
    args = get_args()
    dir_path = os.path.dirname(os.path.realpath(__file__))
    annotation_path = f'{dir_path}/../data/original_claims_anno.jsonl'
    result_path = f'{dir_path}/../data/{args.evidences}/threshold'
    file_names = get_file_names(result_path)
    annotations, annotations_ids = load_annotation(args, annotation_path)
    evaluations = []
    count = 0
    for file_name in file_names:
        file_path = f'{result_path}/{file_name}'
        results = load_data(file_path, annotations_ids)
        scores = []
        score_sum = 0
        evidence_annotations = list()
        evidence_results = list()
        for annotation, result in zip(annotations, results):
            evidence_annotations += annotation['anno'].values()
            keys = list(annotation['anno'].keys())
            keys = list(filter(lambda x: args.evidences in x, keys))
            filtered_evidences = []
            if len(keys) > 0:
                filtered_evidences = filter_evidences(result, keys)
            evidence_results += filtered_evidences
        kf = KFold(n_splits=4, shuffle=False)
        split = kf.split(evidence_results)
        for train, test in tqdm(split):
            test_results = get_split(test, evidence_results)
            test_annotations = get_split(test, evidence_annotations)
            test_results = [labels[x['label']] for x in test_results]
            score = accuracy_score(test_annotations, test_results)
            scores.append(score)
            score_sum += score
        evaluation = {
            'threshold': count * .05 + .3,
            'accuracy': mean(scores),
            'standard deviation': stdev(scores),
            'median': median(scores)
        }
        evaluations.append(evaluation)
        count += 1
    with open(f'{dir_path}/../data/{args.evidences}/threshold_evaluation_{args.evidences}.json', 'w') as fw:
        json.dump(evaluations, fw)
        fw.close()


if __name__ == "__main__":
    main()
