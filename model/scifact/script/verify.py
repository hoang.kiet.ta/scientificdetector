import argparse
from subprocess import call
import os
import torch
import json
import ipdb

from tqdm import tqdm
from model.scifact.verisci.covid import ArxivAbstractRetriever, AbstractRetriever, CordAbstractRetriever, RationaleSelector, LabelPredictor


def get_args():
    parser = argparse.ArgumentParser(
        description="Verify a claim against the CORD-19 corpus.")
    parser.add_argument("claim", type=str, default="res/claims/potential_claims/cleaned_claim.json", help="The claim to be verified")
    parser.add_argument("report_file", type=str, default="covid-report",
                        help="The file where the report will be written (no file extension).")
    parser.add_argument("--evidences", type=str, choices=["none", "arxiv", "biorxiv", "medrxiv"], default="none")
    parser.add_argument("--n_documents", type=int, default=20,
                        help="The number of documents to retrieve from Covidex.")
    parser.add_argument("--rationale_selection_method", type=str,
                        choices=["topk", "threshold"], default="threshold",
                        help="Select top k rationales, or keep sentences with sigmoid scores above `rationale_threshold`.")
    parser.add_argument("--output_format", type=str,
                        choices=["pdf", "markdown"], default="markdown",
                        help="Output format. PDF requires Pandoc.")
    parser.add_argument("--rationale_threshold", type=float, default=0.5,
                        help="Classification threshold for selecting rationale sentences.")
    parser.add_argument("--label_threshold", type=float, default=0.5,
                        help="Classification threshold for label score.")
    parser.add_argument("--keep_nei", action="store_true",
                        help="Keep examples labeled `NOT_ENOUGH_INFO`, for which evidence was identified.")
    parser.add_argument("--negated", action="store_true",
                        help="Verify negated claims")
    parser.add_argument("--coref", action="store_true",
                        help="Verify coreferenced claims")
    parser.add_argument("--full_abstract", action="store_true",
                        help="Show full abstracts, not just evidence sentences.")
    parser.add_argument("--verbose", action="store_true",
                        help="Verbose model output.")
    parser.add_argument("--device", type=str, default=None,
                        help="Device to use. Defaults to `gpu` if one is available, else `cpu`.")
    parser.add_argument("--threshold_exp", action="store_true", help="Conduct parameter tuning.")
    return parser.parse_args()


def inference(args, rationale_threshold):
    # Initialize pipeline components
    if args.verbose:
        print("Initializing model.")
    dir_path = os.path.dirname(os.path.realpath(__file__))
    rationale_selection_model = dir_path + '/../model/rationale_scibert_scifact'
    # rationale_selection_model = dir_path + '/../model/rationale_roberta_large_scifact'
    label_prediction_model = dir_path + '/../model/label_roberta_large_fever_scifact'
    # Get device.
    if args.device is None:
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    else:
        device = torch.device(args.device)

    abstract_retriever = AbstractRetriever()
    rationale_selector = RationaleSelector(rationale_selection_model,
                                           args.rationale_selection_method,
                                           rationale_threshold,
                                           device)
    label_predictor = LabelPredictor(label_prediction_model,
                                     args.keep_nei,
                                     args.label_threshold,
                                     device)

    # Run model.
    evidences = []
    with open(f'{dir_path}/../../../{args.claim}', encoding='utf-8') as f:
        json_output = json.load(f)
        #data_inputs = json_output['data']
        data_inputs = json_output
    if args.verbose:
        print("Retrieving abstracts.")
    count = 1
    if args.evidences == 'none':
        for data_input in data_inputs:
            claim_data = data_input['claims']
            for data in claim_data:
                print(data['claim'])
                print(count)
                if data['checkworthy'] == 1:
                    try:

                        results = abstract_retriever(data['claim'], k=args.n_documents)
                        evidences.append(results)
                        count += 1
                    except IndexError:
                        count += 1
                        continue
            with open(f'{dir_path}/../data/evidences.json', 'w') as out:
                json_data = {
                    "video_title": data_input['title'],
                    "evidences": evidences,
                }
                json.dump(json_data, out)
    if args.evidences != 'none':
        evidence_path = f'{dir_path}/../data/{args.evidences}/evidences'
        if args.negated:
            evidence_path += f'_{args.evidences}_negated.json'
        elif args.coref:
            evidence_path += f'_{args.evidences}_coref_filtered.json'
        else:
            evidence_path += f'_{args.evidences}.json'
        with open(evidence_path) as f:
            evidence_datas = json.load(f)
            print(evidence_datas)
            f.close()
    data_results = []
    if args.negated:
        data_list = []
        claim_ids = []
        for data_input in data_inputs:
            if data_input['negated_claim']:
                claim_pair = data_input['claim'], data_input['negated_claim']
                claim_id = data_input['id']
                data_list.append(claim_pair)
                claim_ids.append(claim_id)

        evidences = evidence_datas
        verification_results = []
        for evidence in tqdm(evidences):
            claim_id = evidence['id']
            negated = evidence['negated']
            claim = evidence['claim']
            documents = evidence['sources']
            print(negated)
            if args.verbose:
                print("Selecting rationales.")
            results = rationale_selector(claim, documents)
            if args.verbose:
                print("Making label predictions")
            results = label_predictor(claim, results)
            print(results)
            json_data = {
                "id": claim_id,
                "claim": claim,
                "evidences": results,
            }
            verification_results.append(json_data)
        result = {
            "video_title": '',
            "evidences": verification_results
        }
        data_results.append(result)
    else:
        claim_id = 0
        #for evidence_data in tqdm(evidence_datas):
            #evidences = evidence_data['evidences']
        verification_results = []
        evidences = evidence_datas
        for evidence in tqdm(evidences):
            try:
                claim = evidence['original']
                documents = evidence['sources']
                print(claim)
                if args.verbose:
                    print("Selecting rationales.")
                results = rationale_selector(claim, documents)
                if args.verbose:
                    print("Making label predictions")
                results = label_predictor(claim, results)
                print(results)
                json_data = {
                    "id": claim_id,
                    "claim": claim,
                    "evidences": results,
                }
                verification_results.append(json_data)
                claim_id += 1
            except AssertionError:
                continue

        result = {
            "video_title": '',
            "evidences": verification_results
        }
        data_results.append(result)
    output = {
        "results": data_results
    }

    return output


def write_result(result, f, full_abstract):
    msg = f"#### [{result['title']}]({result['url']})"
    print(msg, file=f)
    ev_scores = [f"{x:0.2f}" for x in result["evidence_confidence"]]
    ev_scores = ", ".join(ev_scores)
    msg = f"**Decision**: {result['label']} (score={result['label_confidence']:0.2f}, evidence scores={ev_scores})\n"
    print(msg, file=f)

    for i, line in enumerate(result["abstract"]):
        # If we're showing the full abstract, show evidence in green.
        if full_abstract:
            msg = (f"- <span style='color:green'>{line}</span>"
                   if i in result["evidence"]
                   else f"- {line}")
            print(msg, file=f)
        else:
            if i in result["evidence"]:
                msg = f"- {line}"
                print(msg, file=f)

    print(file=f)
    print(40 * "-", file=f)
    print(file=f)


def export(args, results):
    claim = args.claim
    report_file = args.report_file
    dir_path = os.path.dirname(os.path.realpath(__file__))
    f = open(f"{dir_path}/../{report_file}.md", "w", encoding='utf-8')
    msg = f"### Claim\n {claim}"
    print(msg, file=f)
    print(file=f)

    msg = "### Evidence\n"
    print(msg, file=f)
    for result in results:
        write_result(result, f, args.full_abstract)

    msg = "## Config\n"
    print(msg, file=f)
    arg_dict = vars(args)
    for k, v in arg_dict.items():
        if isinstance(v, float):
            v = f"{v:0.2f}"
        msg = f"- {k}: {v}"
        print(msg, file=f)

    f.close()
    if args.output_format == "pdf":
        pdf_file = f"{dir_path}/../{report_file}.pdf"
        cmd = ["pandoc", f"{dir_path}/../{report_file}.md", "-o", pdf_file, "-t", "html"]
        print(cmd)
        call(cmd)
        os.remove(f"{dir_path}/../{report_file}.md")


def export_json(result, args):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    with open(f'{dir_path}/../data/{args.evidences}/results.json', 'w') as f:
        json.dump(result, fp=f, indent=2)
        f.close()


def export_json_threshold(result, args, threshold):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    with open(f'{dir_path}/../data/{args.evidences}/threshold/results_{args.evidences}_thresh{threshold}.json', 'w') as f:
        json.dump(result, fp=f, indent=2)
        f.close()


def main():
    args = get_args()
    if args.threshold_exp:
        min_threshold = 0.3
        max_threshold = 0.7
        count = 0
        while min_threshold < max_threshold:
            print(min_threshold)
            results = inference(args, min_threshold)
            export_json_threshold(results, args, count)
            min_threshold += 0.05
            count += 1
    else:
        threshold = 0.5
        results = inference(args, threshold)
        export_json(results, args)


if __name__ == "__main__":
    main()
