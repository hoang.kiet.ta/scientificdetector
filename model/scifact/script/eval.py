import json
import jsonlines
import os
import argparse

from sklearn.metrics import f1_score, classification_report
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score

labels = {
    'REFUTE': -1,
    'NOT_ENOUGH_INFO': 0,
    'SUPPORT': 1
}


def get_args():
    parser = argparse.ArgumentParser(
        description="Evaluate results")
    parser.add_argument("--evidences", type=str, choices=["arxiv", "biorxiv", "medrxiv"], default="none")
    return parser.parse_args()


def load_data(args, result_path, annotation_path):
    with open(result_path, encoding='utf-8') as f:
        results = json.load(f)['results'][0]['evidences']
        print(results)
        results = [
            {'id': x['id'], 'claim': x['claim'], 'evidences': x['evidences']}
            for x in results
        ]
        f.close()
    with open(annotation_path, encoding='utf-8') as fc:
        annotations = list()
        annotation_ids = list()
        for annotation in jsonlines.Reader(fc):
            annotation_dict = dict([
                (x, y)
                for x, y in zip(annotation['anno'].keys(), annotation['anno'].values()) if args.evidences in x
            ])
            annotations.append({
                'claim_id': annotation['claim_id'],
                'anno': annotation_dict
            })
            annotation_ids.append(annotation['claim_id'])
        fc.close()
    print(results)
    results = [x for x in results if x['id'] in annotation_ids]

    return results, annotations


def filter_evidences(result, annotations_keys):
    if len(result['evidences']) > 0:
        filtered_evidences = [x for x in result['evidences'] if x['url'] in annotations_keys]
        difference = len(annotations_keys) - len(filtered_evidences)
        if difference > 0:
            filtered_evidences += [{'label': 'NOT_ENOUGH_INFO'}] * difference
    else:
        filtered_evidences = [{'label': 'NOT_ENOUGH_INFO'}] * len(annotations_keys)
    return filtered_evidences


def main():
    args = get_args()
    dir_path = os.path.dirname(os.path.realpath(__file__))
    annotation_path = f'{dir_path}/../data/negated_claims_anno.jsonl'
    result_path = f'{dir_path}/../data/{args.evidences}/results_{args.evidences}_roberta_negated.json'
    results, annotations = load_data(args, result_path, annotation_path)
    evidence_annotations = list()
    evidence_results = list()
    print(len(annotations), len(results))
    for annotation, result in zip(annotations, results):
        evidence_annotations += annotation['anno'].values()
        keys = list(annotation['anno'].keys())
        keys = list(filter(lambda x: args.evidences in x, keys))
        filtered_evidences = []
        if len(keys) > 0:
            filtered_evidences = filter_evidences(result, keys)
        evidence_results += filtered_evidences
    evidence_results = [labels[x['label']] for x in evidence_results]
    print(len(evidence_results), len(evidence_annotations))
    print(evidence_results)
    print(evidence_annotations)
    report = classification_report(evidence_annotations, evidence_results, labels=[-1, 0, 1], digits=4, output_dict=True)
    with open(f'{dir_path}/../data/{args.evidences}/evaluations_{args.evidences}_roberta_negated.json', 'w') as fw:
        json.dump(report, fw)
        fw.close()


if __name__ == "__main__":
    main()
