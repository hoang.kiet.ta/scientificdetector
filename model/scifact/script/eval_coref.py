import json
import jsonlines
import os
import argparse
import numpy as np

from sklearn.metrics.pairwise import cosine_similarity


labels = {
    'REFUTE': -1,
    'NOT_ENOUGH_INFO': 0,
    'SUPPORT': 1
}


def get_args():
    parser = argparse.ArgumentParser(
        description="Evaluate results")
    parser.add_argument("--evidences", type=str, choices=["arxiv", "biorxiv", "medrxiv"], default="none")
    return parser.parse_args()


def load_data(args, original_path, coref_path):
    with open(original_path) as of:
        original_results = json.load(of)['results'][0]['evidences']
        of.close()
    with open(coref_path) as cf:
        coref_results = json.load(cf)['results'][0]['evidences']
        cf.close()
    annotations, ids = load_annotations(args)

    original_results = [x for x in original_results if x['id'] in ids]
    coref_results = [x for x in coref_results if x['id'] in ids]
    return original_results, coref_results, annotations


def load_annotations(args):

    dir_path = os.path.dirname(os.path.realpath(__file__))
    annotation_path = f'{dir_path}/../data/negated_claims_anno.jsonl'

    with open(annotation_path, encoding='utf-8') as fc:
        annotations = list()
        annotation_ids = list()
        for annotation in jsonlines.Reader(fc):
            annotation_dict = dict([
                (x, y)
                for x, y in zip(annotation['anno'].keys(), annotation['anno'].values()) if args.evidences in x
            ])
            annotations.append({
                'claim_id': annotation['claim_id'],
                'anno': annotation_dict
            })
            annotation_ids.append(annotation['claim_id'])
        fc.close()
    return annotations, annotation_ids


def calc_cosine(o_confidences, c_confidences):
    evaluation_list = []
    for oc, cc in zip(o_confidences, c_confidences):
        claim = oc['claim']
        claim_id = oc['id']
        olabel_confidence = oc['label_confidence']
        clabel_confidence = cc['label_confidence']
        cosine = cosine_similarity(np.array(olabel_confidence).reshape(1,-1), np.array(clabel_confidence).reshape(1,-1))
        evaluation = {
            "id": claim_id,
            "claim": claim,
            "olabel_confidence": olabel_confidence,
            "clabel_confidence": clabel_confidence,
            "cosine": cosine[0][0],
        }
        evaluation_list.append(evaluation)
    return evaluation_list


def main():
    args = get_args()
    dir_path = os.path.dirname(os.path.realpath(__file__))

    original_path = f'{dir_path}/../data/{args.evidences}/results_{args.evidences}_original_negated.json'
    coref_path = f'{dir_path}/../data/{args.evidences}/results_{args.evidences}_negated_thresh.json'
    oresults, cresults, annotations = load_data(args, original_path, coref_path)

    o_evidence_label_confidences = list()
    for annotation, oresult in zip(annotations, oresults):
        keys = list(annotation['anno'].keys())
        keys = list(filter(lambda x: args.evidences in x, keys))
        filtered_evidences = [x for x in oresult['evidences'] if x['url'] in keys]
        refute = 0.0
        nei = 0.0
        support = 0.0
        if len(filtered_evidences) > 0:
            for evidence in filtered_evidences:
                label_confidence = evidence['label_confidence']
                refute += label_confidence[0]
                nei += label_confidence[1]
                support += label_confidence[2]
            refute /= len(filtered_evidences)
            nei /= len(filtered_evidences)
            support /= len(filtered_evidences)
        else:
            nei = 1.0
        label_confidence_obj = {
            "id": annotation['claim_id'],
            "claim": oresult['claim'],
            "label_confidence": [refute, nei, support]
        }
        o_evidence_label_confidences.append(label_confidence_obj)
    c_evidence_label_confidences = list()
    for annotation, cresult in zip(annotations, cresults):
        keys = list(annotation['anno'].keys())
        keys = list(filter(lambda x: args.evidences in x, keys))
        filtered_evidences = [x for x in cresult['evidences'] if x['url'] in keys]
        refute = 0.0
        nei = 0.0
        support = 0.0
        if len(filtered_evidences) > 0:
            for evidence in filtered_evidences:
                label_confidence = evidence['label_confidence']
                refute += label_confidence[0]
                nei += label_confidence[1]
                support += label_confidence[2]
            refute /= len(filtered_evidences)
            nei /= len(filtered_evidences)
            support /= len(filtered_evidences)
        else:
            nei = 1.0
        label_confidence_obj = {
            "id": annotation['claim_id'],
            "claim": cresult['claim'],
            "label_confidence": [refute, nei, support]
        }
        c_evidence_label_confidences.append(label_confidence_obj)
    evaluations = calc_cosine(o_evidence_label_confidences, c_evidence_label_confidences)

    with open(f'{dir_path}/../data/{args.evidences}/cosine_arxiv_negated.json', 'w') as fw:
        json.dump(evaluations, fw)
        fw.close()


if __name__ == "__main__":
    main()
