import json
import os
import argparse


def get_args():
    parser = argparse.ArgumentParser(
        description="Print results")
    parser.add_argument("--evidences", type=str, choices=["none", "arxiv", "biorxiv", "medrxiv"], default="none")
    parser.add_argument("--rational", type=str, choices=["roberta", "scibert"], default="scibert")
    parser.add_argument("--predictor", type=str, choices=["roberta"], default="roberta")

    return parser.parse_args()


def print_best_stats(args):
    dir_path = os.path.dirname(os.path.realpath(__file__))

    search_engine = args.evidences

    best_sentence_so_far = ('', 0.0, 0.0)
    best_sentence_label = 'NOT_ENOUGH_INFO'
    best_claim = ''
    worst_sentence_so_far = ('', 1.0, 1.0)
    worst_sentence_label = 'NOT_ENOUGH_INFO'
    worst_claim = ''
    # model = f'{args.evidences}_{args.rational}_{args.predictor}'
    model = f'{args.evidences}'
    with open(f'{dir_path}/../data/{search_engine}/results_{model}.json') as f:
        json_out = json.load(f)
        results = json_out['results']
        f.close()
    for result in results:
        evidences = result['evidences']
        if len(evidences) > 0:
            for evidence in evidences:
                claim = evidence['claim']
                verified_evidences = evidence['evidences']
                if len(verified_evidences) > 0:
                    for verified_evidence in verified_evidences:
                        positions = verified_evidence['evidence']
                        abstract = verified_evidence['abstract']
                        confidence = verified_evidence['evidence_confidence']
                        label = verified_evidence['label']
                        label_confidence = max(verified_evidence['label_confidence'])
                        rationals = list(zip(positions, confidence))

                        if best_sentence_so_far[2] < label_confidence and label != 'NOT_ENOUGH_INFO':
                            best_claim = claim
                            max_rational = max(rationals, key=lambda tup: tup[1])
                            best_sentence_so_far = abstract[max_rational[0]], max_rational[1], label_confidence
                            best_sentence_label = label
                        if worst_sentence_so_far[2] > label_confidence:
                            worst_claim = claim
                            max_rational = max(rationals, key=lambda tup: tup[1])
                            worst_sentence_so_far = abstract[max_rational[0]], max_rational[1], label_confidence
                            worst_sentence_label = label
    print(f'BEST CLAIM: {best_claim}')
    print(f'BEST SENTENCE: {best_sentence_so_far[0]}, {best_sentence_so_far[1]}')
    print(f'BEST LABEL: {best_sentence_label}, {best_sentence_so_far[2]}')
    print(f'WORST CLAIM: {worst_claim}')
    print(f'WORST SENTENCE: {worst_sentence_so_far[0]}, {best_sentence_so_far[1]}')
    print(f'WORST LABEL: {worst_sentence_label}, {worst_sentence_so_far[2]}')


def print_result(args):
    dir_path = os.path.dirname(os.path.realpath(__file__))

    search_engine = args.evidences

    if search_engine != 'none':
        with open(f'{dir_path}/../data/{search_engine}/results.json') as f:
            json_out = json.load(f)
            results = json_out['results']
            f.close()
        for result in results:
            title = result['video_title']
            evidences = result['evidences']
            if len(evidences) > 0:
                print(f'video title: {title}')
                for evidence in evidences:
                    claim = evidence['claim']
                    verified_evidences = evidence['evidences']
                    if len(verified_evidences) > 0:
                        print(f'claim: {claim}')
                        for verified_evidence in verified_evidences:
                            paper_title = verified_evidence['title']
                            positions = verified_evidence['evidence']
                            abstract = verified_evidence['abstract']
                            confidence = verified_evidence['evidence_confidence']
                            label = verified_evidence['label']
                            print(f'paper title: {paper_title}')
                            rationals = list(zip(positions, confidence))
                            rationals.sort(key=lambda tup: tup[1], reverse=True)
                            print(sorted(confidence, key=float, reverse=True))
                            for r in rationals:
                                print(f'- {abstract[r[0]]}')
                            print(f'label: {label}')
                        print(f'{"-" * 50}')


def main():
    args = get_args()
    print_best_stats(args)


if __name__ == "__main__":
    main()
