import json
import os
import argparse
import spacy
from tqdm import tqdm


def get_args():
    parser = argparse.ArgumentParser(
        description="Print results")
    parser.add_argument("--evidences", type=str, choices=["none", "arxiv", "biorxiv", "medrxiv"], default="none")
    parser.add_argument("--coref", action="store_true", help="retrieve evidences from coref claims")

    return parser.parse_args()


def sentencize(text):
    nlp = spacy.load("en_core_sci_sm")
    doc = nlp(text)
    return [sent.text for sent in doc.sents]


def sentencize_without_coref(data):
    claims = data['evidences']
    claim_list = []
    for claim in tqdm(claims):
        evidences = claim['sources']
        evidences_list = []
        for evidence in tqdm(evidences):
            abstract = sentencize(evidence['abstract'])
            evidence_data = {
                "title": evidence['title'],
                "abstract": abstract,
                "url": evidence['url'],
                "doi": evidence['doi']
            }
            evidences_list.append(evidence_data)
        claim_data = {
            "claim": claim['claim'],
            "sources": evidences_list
        }
        claim_list.append(claim_data)
    data_out = {
        "video_title": data['video_title'],
        "evidences": claim_list
    }
    return data_out


def sentencize_with_coref(data):
    claims = data['evidences']
    claim_list = []
    for claim in tqdm(claims):
        evidences = claim['sources']
        evidences_list = []
        for evidence in tqdm(evidences):
            abstract = sentencize(evidence['abstract'])
            evidence_data = {
                "title": evidence['title'],
                "abstract": abstract,
                "url": evidence['url'],
                "doi": evidence['doi'],
            }
            evidences_list.append(evidence_data)
        claim_data = {
            "claim": claim['claim'],
            "sources": evidences_list,
            "negated": claim['negated']
        }
        claim_list.append(claim_data)
    data_out = {
        "video_title": '',
        "evidences": claim_list
    }
    return data_out


def main():
    args = get_args()
    dir_path = os.path.dirname(os.path.realpath(__file__))
    if args.evidences != 'none' and not args.coref:
        with open(f'{dir_path}/../data/{args.evidences}/evidences.json') as f:
            dataset = json.load(f)
            f.close()
    else:
        with open(f'{dir_path}/../data/{args.evidences}/evidences_coref.json') as f:
            dataset = json.load(f)
            f.close()

    data_list = []
    for data in tqdm(dataset):
        if args.coref:
            data_out = sentencize_with_coref(data)
        else:
            data_out = sentencize_without_coref(data)
        data_list.append(data_out)
    with open(f'{dir_path}/../data/{args.evidences}/evidences_test.json', 'w') as out:
        json.dump(data_list, out)
        out.close()


if __name__ == '__main__':
    main()
