import jsonlines
import json
import os


def get_negated_evidences(negated_claims, keys):
    data_list = []
    dir_path = os.path.dirname(os.path.realpath(__file__))
    with open(f'{dir_path}/../data/arxiv/evidences_arxiv.json') as af:
        arxiv = json.load(af)[0]['evidences']
        af.close()
    with open(f'{dir_path}/../data/biorxiv/evidences_biorxiv.json') as bf:
        biorxiv = json.load(bf)[0]['evidences']
        bf.close()
    with open(f'{dir_path}/../data/medrxiv/evidences_medrxiv.json') as mf:
        medrxiv = json.load(mf)[0]['evidences']
        mf.close()

    filtered_negated = [
        (y['original'], y['negated_claim'], y['id'])
        for y in negated_claims
        if y['id'] in keys
    ]
    evidences_arxiv, evidences_biorxiv, evidences_medrxiv = [], [], []
    for claim, negated, claim_id in filtered_negated:
        claim_source_arxiv = [x['sources'] for x in arxiv if x['claim'] == claim]
        if len(claim_source_arxiv) > 0:
            claim_source_arxiv = claim_source_arxiv[0]
        claim_evidences_arxiv = {
            "id": claim_id,
            "claim": claim,
            "negated": negated,
            "sources": claim_source_arxiv
        }
        evidences_arxiv.append(claim_evidences_arxiv)

        claim_source_biorxiv = [x['sources'] for x in biorxiv if x['claim'] == claim]
        if len(claim_source_biorxiv) > 0:
            claim_source_biorxiv = claim_source_biorxiv[0]
        claim_evidences_biorxiv = {
            "id": claim_id,
            "claim": claim,
            "negated": negated,
            "sources": claim_source_biorxiv
        }
        evidences_biorxiv.append(claim_evidences_biorxiv)

        claim_source_medrxiv = [x['sources'] for x in medrxiv if x['claim'] == claim]
        if len(claim_source_medrxiv) > 0:
            claim_source_medrxiv = claim_source_medrxiv[0]
        claim_evidences_medrxiv = {
            "id": claim_id,
            "claim": claim,
            "negated": negated,
            "sources": claim_source_medrxiv
        }
        evidences_medrxiv.append(claim_evidences_medrxiv)

    return evidences_arxiv, evidences_biorxiv, evidences_medrxiv


def main():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    data_path = f'{dir_path}/../data'
    negated_claims_path = f'{dir_path}/../../../res/claims/potential_claims/negated_claims.json'
    claims_annotation_path = f'{data_path}/original_claims_anno.jsonl'
    with open(negated_claims_path, encoding='utf-8') as f:
        negated_claims = json.load(f)
        negated_claims = [x for x in negated_claims if x['negated_claim'] != None]
        f.close()
    claim_annotations = []
    with open(claims_annotation_path, encoding='utf-8') as fc:
        for annotation in jsonlines.Reader(fc):
            claim_annotations.append(annotation)
        fc.close()
    claim_keys = [y['claim_id'] for y in claim_annotations]
    filtered_claim_keys = [x['id'] for x in negated_claims if x['id'] in claim_keys]
    filtered_negated_claims = [x for x in negated_claims if x['id'] in filtered_claim_keys]
    negated = []
    for negated_claim in filtered_negated_claims:
        claim_id = negated_claim['id']
        negated += [x for x in claim_annotations if x['claim_id'] == claim_id]
    links = []
    for n in negated:
        claim_id = n['claim_id']
        urls = n['anno'].keys()

        arxiv_links = []
        biorxiv_links = []
        medrxiv_links = []

        for url in urls:
            if 'arxiv' in url:
                arxiv_links.append(url)
            elif 'biorxiv' in url:
                biorxiv_links.append(url)
            else:
                medrxiv_links.append(url)
        link = {
            'id': claim_id,
            'arxiv': arxiv_links,
            'biorxiv': biorxiv_links,
            'medrxiv': medrxiv_links
        }
        links.append(link)

    arxiv, biorxiv, medrxiv = get_negated_evidences(negated_claims, claim_keys)

    count = 0

    res_arxiv_evidences = []
    res_biorxiv_evidences = []
    res_medrxiv_evidences = []

    print(len(links), len(arxiv), len(biorxiv), len(medrxiv))
    for link, arx, brx, mrx in zip(links, arxiv, biorxiv, medrxiv):
        print(link)
        arx_sources = arx['sources']
        brx_sources = brx['sources']
        mrx_sources = mrx['sources']
        claim = arx['claim']
        negated = arx['negated']
        negated_arxiv_evidences = [x for x in arx_sources if x['url'] in link['arxiv']]
        negated_biorxiv_evidences = [x for x in brx_sources if x['url'] in link['biorxiv']]
        negated_medrxiv_evidences = [x for x in mrx_sources if x['url'] in link['medrxiv']]

        res_arxiv_evidences.append({"id": link['id'], "claim": claim, "negated": negated,"sources": negated_arxiv_evidences})
        res_biorxiv_evidences.append({"id": link['id'], "claim": claim, "negated": negated, "sources": negated_biorxiv_evidences})
        res_medrxiv_evidences.append({"id": link['id'], "claim": claim, "negated": negated, "sources": negated_medrxiv_evidences})

    with open(f'{data_path}/arxiv/evidences_arxiv_negated.json', 'w') as f:
        json.dump(res_arxiv_evidences, f)
        f.close()
    with open(f'{data_path}/biorxiv/evidences_biorxiv_negated.json', 'w') as f:
        json.dump(res_biorxiv_evidences, f)
        f.close()
    with open(f'{data_path}/medrxiv/evidences_medrxiv_negated.json', 'w') as f:
        json.dump(res_medrxiv_evidences, f)
        f.close()


if __name__ == "__main__":
    main()
