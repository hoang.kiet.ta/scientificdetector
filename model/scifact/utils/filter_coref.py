import os
import json
import argparse


def get_args():
    parser = argparse.ArgumentParser(
        description="Evaluate results")
    parser.add_argument("--evidences", type=str, choices=["arxiv", "biorxiv", "medrxiv"], default="none")
    return parser.parse_args()


def get_data(args, coref_claims):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    evidences_path = f'{dir_path}/../data/{args.evidences}/evidences_{args.evidences}_coref.json'

    with open(evidences_path) as f:
        evidences = json.load(f)[0]['evidences']
    evidences = [
        {
            "id": i,
            "claim": x['claim'],
            "original": y['claim'],
            "sources": x['sources']
        }
        for x, y, i in zip(evidences, coref_claims, range(len(evidences)))
    ]

    return evidences


def get_claims():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    coref_path = f'{dir_path}/../../../res/claims/potential_claims/negated_claims.json'

    with open(coref_path) as f:
        coref_data = json.load(f)

    coref_claims = list()
    count = 0
    for data in coref_data:
        if data['changed']:
            claim_id = count
            coref_claim = data['claim']
            original = data['original']
            coref_claim_obj = {
                "id": claim_id,
                "coref": coref_claim,
                "claim": original
            }
            coref_claims.append(coref_claim_obj)
            count += 1
    return coref_claims


def export(args, evidences):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    output_path = f'{dir_path}/../data/{args.evidences}/evidences_{args.evidences}_coref_filtered.json'

    with open(output_path, 'w') as fw:
        json.dump(evidences, fw)
        fw.close()


def main():
    args = get_args()
    coref_claims = get_claims()
    evidences = get_data(args, coref_claims)
    export(args, evidences)


if __name__ == "__main__":
    main()
