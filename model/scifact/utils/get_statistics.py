import argparse
import json
import os


def get_args():
    parser = argparse.ArgumentParser(
        description="Print results")
    parser.add_argument("--evidences", type=str, choices=["none", "arxiv", "biorxiv", "medrxiv"], default="none")

    return parser.parse_args()


def get_statistics(args):
    dir_path = os.path.dirname(os.path.realpath(__file__))

    search_engine = args.evidences

    if search_engine != 'none':
        supportive_count = 0
        refutive_count = 0
        highest_confidence = ('', '', 0.0)
        lowest_confidence = ('', '', 1.0)
        with open(f'{dir_path}/../data/{search_engine}/results.json') as f:
            json_out = json.load(f)
            results = json_out['results']
            f.close()
        for result in results:
            title = result['video_title']
            evidences = result['evidences']
            for evidence in evidences:
                claim = evidence['claim']
                verified_evidences = evidence['evidences']
                if len(verified_evidences) > 0:
                    for verified_evidence in verified_evidences:
                        positions = verified_evidence['evidence']
                        abstract = verified_evidence['abstract']
                        confidence = verified_evidence['evidence_confidence']
                        label = verified_evidence['label']
                        rationals = list(zip(positions, confidence))
                        rationals.sort(key=lambda tup: tup[1], reverse=True)
                        max_confidence = rationals[0][1]
                        min_confidence = rationals[-1][1]
                        index = rationals[0][0]
                        index2 = rationals[-1][0]
                        if max_confidence > highest_confidence[2]:
                            highest_confidence = claim, abstract[index], max_confidence, abstract[index2], min_confidence, label
                        if min_confidence < lowest_confidence[2]:
                            lowest_confidence = claim, abstract[index], max_confidence, abstract[index2], min_confidence, label
                        if label == 'SUPPORT':
                            supportive_count += 1
                        if label == 'REFUTE':
                            refutive_count += 1
        h_claim, h_rational, h_confidence, h_min_rational, h_min, h_label = highest_confidence
        l_claim, l_rational, l_confidence, l_min_rational, l_min, l_label = lowest_confidence
        print(f'Supportives: {supportive_count}')
        print(f'Refutives: {refutive_count}')
        print(f'Highest Confident Rational:\nClaim: {h_claim}\n- {h_rational}\n- {h_confidence}\n- {h_min_rational}\n- {h_min}\n- {h_label}')
        print(f'Lowest Confident Rational:\nClaim: {l_claim}\n- {l_rational}\n- {l_confidence}\n- {l_min_rational}\n- {l_min}\n- {l_label}')


def main():
    args = get_args()
    get_statistics(args)


if __name__ == "__main__":
    main()
