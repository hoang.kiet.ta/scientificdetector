"""
Example script to predict claim using trained CRF claim model
based on pre-trained CRF model from structured abstract
"""
import os
import sys
sys.path.insert(0, '..')

from nltk import sent_tokenize
import torch
import json
from torch.nn.modules.linear import Linear

from allennlp.models.archival import load_archive
from allennlp.predictors import Predictor
from allennlp.common.file_utils import cached_path
from allennlp.common.util import JsonDict
from allennlp.data import Instance
from allennlp.modules import TimeDistributed, ConditionalRandomField

from discourse import *

DISCOURSE_MODEL_PATH = 'https://detecting-scientific-claim.s3-us-west-2.amazonaws.com/model.tar.gz'
WEIGHT_PATH = 'https://detecting-scientific-claim.s3-us-west-2.amazonaws.com/model_crf_tf.th'
archive = load_archive(DISCOURSE_MODEL_PATH)
discourse_predictor = Predictor.from_archive(archive, 'discourse_crf_predictor')
EMBEDDING_DIM = 300


class ClaimCrfPredictor(Predictor):
    """"
    Predictor wrapper for the AcademicPaperClassifier
    """
    def _json_to_instance(self, json_dict: JsonDict) -> Instance:
        sentences = json_dict['sentences']
        instance = self._dataset_reader.text_to_instance(sents=sentences)
        return instance


def predict(transcription_path, output_path):
    model = discourse_predictor._model
    for param in list(model.parameters()):
        param.requires_grad = False
    num_classes, constraints, include_start_end_transitions = 2, None, False
    model.crf = ConditionalRandomField(num_classes, constraints,
                                       include_start_end_transitions=include_start_end_transitions)
    model.label_projection_layer = TimeDistributed(Linear(2 * EMBEDDING_DIM, num_classes))
    print(model)
    model.load_state_dict(torch.load(cached_path(WEIGHT_PATH), map_location=torch.device('cpu')))

    reader = CrfPubmedRCTReader()
    claim_predictor = ClaimCrfPredictor(model, dataset_reader=reader)

    file_names = list()

    for file in os.listdir(transcription_path):
        if file.endswith('.txt'):
            file_names.append(file)
    potential_claims = list()
    for file_name in file_names:
        r = open(transcription_path + '/' + file_name, mode='r+')
        pred_list = []
        sentences = sent_tokenize(r.read())
        if len(sentences) > 0:
            print(sentences)
            instance = reader.text_to_instance(sents=sentences)
            pred = claim_predictor.predict_instance(instance)
            logits = torch.FloatTensor(pred['logits'])
            best_paths = model.crf.viterbi_tags(logits.unsqueeze(0),
                                                torch.LongTensor(pred['mask']).unsqueeze(0))
            pred_list.append(best_paths[0][0])
            print(pred_list)
            indeces = [i for i, p in enumerate(pred_list[0]) if p == 1]

            for index in indeces:
                potential_claims.append(sentences[index])
    with open(os.path.join(output_path, 'claims.json'), mode='w+') as wr:
        json.dump(potential_claims, wr)
        wr.close()
