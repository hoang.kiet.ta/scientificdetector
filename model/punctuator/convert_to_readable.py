import sys
import os
from io import open
from model.punctuator import data


def convert_to_readable(processed_path, readable_path):

    file_names = list()

    for file in os.listdir(processed_path):
        if file.endswith('.txt'):
            file_names.append(file)

    for file_name in file_names:
        input_file = processed_path + '/' + file_name
        output_file = readable_path + '/' + file_name.split('-transcribed')[0] + '.txt'

        with_newlines = len(sys.argv) > 3 and bool(int(sys.argv[3]))

        with open(input_file, 'r', encoding='utf-8') as in_f, open(output_file, 'w', encoding='utf-8') as out_f:
            last_was_eos = True
            first = True
            for token in in_f.read().split():
                if token in data.PUNCTUATION_VOCABULARY:
                    out_f.write(token[:1])
                else:
                    out_f.write(('' if first else ' ') + (token.title() if last_was_eos else token))

                last_was_eos = token in data.EOS_TOKENS
                if with_newlines and last_was_eos:
                    out_f.write('\n')
                    first = True
                else:
                    first = False


if __name__ == "__main__":
    convert_to_readable()