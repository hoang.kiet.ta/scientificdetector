import numpy as np
import os
import json
from statistics import mean, stdev, median
from tqdm import tqdm
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score
from model.claim_spotter.bert_adversarial.core.api.api_wrapper import ClaimSpotterAPI


def get_sentences(test_data):
    sentences = []
    for data in test_data:
        sentences.append(data['claim'])
    return sentences


def get_test_set(indices, sentences):
    sen_list = []
    for index in indices:
        sen_list.append(sentences[index])
    return sen_list


def get_labels(pred):
    ground_truths = []
    predict_list = []
    for pred_data in pred:
        ground_truths.append(pred_data['checkworthy'])
        predict_list.append(pred_data['prediction'])
    return ground_truths, predict_list


if __name__ == '__main__':
    api = ClaimSpotterAPI()
    dir_path = os.path.dirname(os.path.realpath(__file__))

    file_names = list()
    ground_truths_list = []
    ground_truths_path = os.path.join(dir_path, '../../../res/claims/potential_claims/cleaned_claims.json')
    output_path = os.path.join(dir_path, 'evaluation.json')
    evaluations = []
    if os.path.exists(output_path):
        with open(output_path, 'r') as f:
            evaluations = json.load(f)
            f.close()
    threshhold = 0.4
    with open(ground_truths_path, 'r') as fw:
        dataset = json.load(fw)
        ground_truths_data = dataset['data'][0]
        print(ground_truths_data)
        fw.close()

    while threshhold <= 0.6:
        print(threshhold)
        kf = KFold(n_splits=5, shuffle=False)
        split = kf.split(ground_truths_data['claims'])
        scores = []
        score_sum = 0
        for train, test in tqdm(split):
            test_set = get_test_set(test, ground_truths_data['claims'])
            sentence_list = get_sentences(test_set)
            predictions = api.batch_sentence_query(sentence_list)
            pred_list = []
            for i in range(len(sentence_list)):
                worthy = 0
                if predictions[i][1] > threshhold:
                    worthy = 1
                claim_data = {
                    "claim": sentence_list[i],
                    "checkworthy": test_set[i]['checkworthy'],
                    "prediction": worthy,
                    "prob": predictions[i][1]
                }
                pred_list.append(claim_data)
            expected, actual = get_labels(pred_list)
            score = accuracy_score(expected, actual)
            scores.append(score)
            score_sum += score
        evaluation = {
            'threshhold': threshhold,
            'accuracy': mean(scores),
            'standard_deviation': stdev(scores),
            'median': median(scores)
        }
        evaluations.append(evaluation)
        threshhold += 0.005
    with open(output_path, 'w') as fw:
        json.dump(evaluations, fw)
        fw.close()
