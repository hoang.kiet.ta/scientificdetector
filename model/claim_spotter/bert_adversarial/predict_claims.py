# Copyright (C) 2020 IDIR Lab - UT Arlington
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License v3 as published by
#     the Free Software Foundation.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Contact Information:
#     See: https://idir.uta.edu/cli.html
#
#     Chengkai Li
#     Box 19015
#     Arlington, TX 76019
#

import numpy as np
import os
import json
from model.claim_spotter.bert_adversarial.core.api.api_wrapper import ClaimSpotterAPI

if __name__ == '__main__':
    api = ClaimSpotterAPI()
    from_file = False
    print('--- Batch Sentence Query ---')

    dir_path = os.path.dirname(os.path.realpath(__file__))
    transcription_path = f'{dir_path}/../../../res/claims/transcripts/transcripts.json'

    file_names = list()
    claims_list = []
    if from_file:
        with open(f'{transcription_path}', encoding='utf-8') as f:
            json_output = json.load(f)
            json_data = json_output['data']
        for data in json_data:
            title = data['title']
            sentence_list = data['transcripts'].split('.')
            print(title)
            predictions = api.batch_sentence_query(sentence_list)
            claim_out = list()
            for i in range(len(sentence_list)):
                worthy = 0
                if predictions[i][1] > 0.5:
                    worthy = 1
                claim_data = {
                    "claim": sentence_list[i],
                    "checkworthy": worthy,
                    "prob": predictions[i][1]
                }
                claim_out.append(claim_data)
            json_data = {
                "title": title,
                "claims": claim_out,
            }
            claims_list.append(json_data)

    else:
        claims_path = os.path.join(dir_path, '../../../res/claims/cleaned_claims')

        f = open(claims_path, 'r')
        sentence_list = f.read().split('\n')
        predictions = api.batch_sentence_query(sentence_list)
        claim_out = list()
        print(len(sentence_list))
        for i in range(len(sentence_list)):
            worthy = 0
            if predictions[i][1] > 0.5:
                worthy = 1
            claim_data = {
                "claim": sentence_list[i],
                "checkworthy": worthy,
                "prob": predictions[i][1]
            }
            claim_out.append(claim_data)
        json_data = {
            "title": '',
            "claims": claim_out
        }
        claims_list.append(json_data)
    output_json = {
        "data": claims_list
    }
    output_path = os.path.join(dir_path, '../../../res/claims/potential_claims/claims.json')

    with open(output_path, mode='w+') as cw:
        json.dump(output_json, cw)
