# ScientificDetector

## Content
- [Setup](#setup)
- [Download Pretrained Models](#pretrained-model)
- [Download Videos](#download-videos)
- [Transcribe Videos](#transcribe)
- [Detect Claims](#extract-claims)
- [Retrieve Evidence](#retrieve-evidence)
- [Verify Evidence](#verify-evidence)

## Setup

Please run the following command to setup requirements for the project

```shell script
pip install -r requirements.txt
```

## Download pretrained models
You can download the pretrained models for the claim detection and evidence classification task:
### Claim Detection
#### Claimspotter
```shell script
sh \model\claim_spotter\data\get_bert.sh
```
### Evidence Classification
#### Scifact
##### Rationale Selector
- [RoBERTa](https://scifact.s3-us-west-2.amazonaws.com/release/latest/models/rationale_roberta_large_scifact.tar.gz)
- [SciBERT](https://scifact.s3-us-west-2.amazonaws.com/release/latest/models/rationale_scibert_scifact.tar.gz)

##### Label Predictor

- [RoBERTa](https://scifact.s3-us-west-2.amazonaws.com/release/latest/models/label_roberta_large_fever_scifact.tar.gz)

## Preprocessing
Before detecting scientific claims in videos, audio need to be extracted. The following scripts preprocess the YouTube video content
into audio transcripts. The video dataset used in the thesis can be downloaded at [link](https://github.com/JuanCarlosCSE/YouTube_misinfo).



####Configuration File
Additionally, we provide a config file to configure the preprocessing. The config file can be found in ```\preprocessing\config.py```. 


**config**

| Configuration  |  Value   | description | 
| :----- | :------: | :------: |
| ```download_videos``` | True or False |   enable video downloading   | 
| ```create_video_list``` |   True or False   | enable video list creation |
| ```extract_audio``` | True or False |   enable audio extraction   |
| ```split``` | True or False |   enable splitting audio   |
| ```transcribe``` | True or False |   enable audio transcription   |
| ```recombine``` | True or False |   enable recombination of the splitted audio transcription   |
| ```json``` | True or False |   enable json output jormat for ibm   |
| ```punctuate``` | True or False |   enable restoring punctuation of the transcript   |
| ```youtube_api_key``` |  | specify youtube api key |
| ```credential_file``` |  | specify credential file |

#### Download videos
Firstly, videos have to be downloaded. The following script takes ```video ids``` as input and download videos from **YouTube**.
The videos are stored in ```res\videos```
```shell script
\preprocessing\extract_videos.py
```
#### Extract audio
After downloading videos, the next step is to extract audios from the videos. The script takes the ```video path``` as input and
extract the audio files of all the videos. The audio files are then stored in ```res\audios```.
```shell script
\preprocessing\extract_audios.py
```
#### Transcribe videos
After extracting the audio files, the last part of preprocessing is transcribing the audio files. The script takes the ```audio path``` as input
and transcribe the audio files. The transcription files are then stored in ```res\transcripts```.
```shell script
\preprocessing\extract_texts.py
```

### Detect claims
To detect claims, two models have been used to detect claims. The following scripts execute the claim detections.
#### Claim Extractor
```shell script
\preprocessing\extract_claims.py
```
#### Claimspotter

##### Re-train Claimspotter
```shell script
\model\claim_spotter\bert_adversarial\train.py
```

##### Predict Claimspotter
```shell script
\model\claim_spotter\bert_adversarial\predict_claims.py
```

### Retrieve evidence
To retrieve the corresponding evidence of each claim the following scripts can be executed. We provided for ```arxiv```,
```biorxiv```, ```medrxiv``` scripts to retrieve evidence.
#### Retrieve arxiv evidence
```shell script
\model\scifact\script\retrieve_arxiv.py [--coref]
```

Optionale parameters:

```--coref``` retrieve evidence of coreferenced claims

#### Retrieve biorxiv evidence
```shell script
\model\scifact\script\retrieve_biorxiv.py [--coref]
```

Optionale parameters:

```--coref``` retrieve evidence of coreferenced claims

#### Retrieve medrxiv evidence
```shell script
\model\scifact\script\retrieve_medrxiv.py [--coref]
```

Optionale parameters:

```--coref``` retrieve evidence of coreferenced claims

### Verify evidence
```shell script
\model\scifact\script\verify.py
    [claim-path]
    [report-path]
    --evidences [arxiv|biorxiv|medrxiv]
    [--keep_nei]
    [--rationale_selection_method] [topk|threshold]
    [--negated|--coref]
    [--rationale_threshold] [threshold]
```
Optionale parameters:

```--keep_nei``` enable NEI label keeping

```--negated``` enable inference evidence of negated claims

```--coref```  enable inference evidence of coreferenced claims

```--rationale_selection_method [topk, threshold]``` select the sentence selection strategy (default: ```threshold```) 

```--rationale_threshold [threshold]``` select the threshold for selecting a sentence (default: ```0.5```)
